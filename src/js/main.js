// mobile nav

let body = document.querySelector('body'),
mobileBtnOpen = document.querySelector('.mobile-btn'),
mobileBtnClose = document.querySelector('.mobile-close'),
mobileMenu = document.querySelector('.main-nav__top-nav'),
// orderBtn = document.getElementsByClassName('.one-type__callback'),
modalBtn = document.querySelector('.one-type__callback'),
topCallback = document.querySelector('.main-nav__callback'),
mobileCallback = document.querySelector('.top-callback--mobile'),
modalClose = document.querySelector('.modal__close'),
modalBg = document.querySelector('.modal__bg'),
callbackTrade1 = document.querySelector('.personal-tariff__text--link');
callbackTrade2 = document.querySelector('.trade__callback');


mobileBtnOpen.addEventListener('click', mobileMenuAction);
// mobileBtnOpen.addEventListener('click', alert('click'));
mobileBtnClose.addEventListener('click', mobileMenuAction);

function mobileMenuAction(){
	body.classList.toggle('mobile-open');
	mobileMenu.classList.toggle('top-nav--mobile-open');
}

// modal

// orderBtn.addEventListener('click', openModal);
if (modalBtn) {
	modalBtn.addEventListener('click', openModal);
}
topCallback.addEventListener('click', openModal);
mobileCallback.addEventListener('click', openModal);
modalClose.addEventListener('click', openModal);
modalBg.addEventListener('click', openModal);
if (callbackTrade1){
	callbackTrade1.addEventListener('click', openModal);
	callbackTrade2.addEventListener('click', openModal);
}

function openModal(){
	body.classList.toggle('modal--open');
}